﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11TodoEF
{
    [Table("Todos")]
    public class Todo
    {
        public int Id { get; set; }

        [Required] // non-null
        [StringLength(200)] //nvacrchar(200)
        public string Task { get; set; }

        //[Required] // non-null
        //public int Difficulty { get; set; }

        [Required] // non-null
        public DateTime DueDate { get; set; }

        [NotMapped] // make sure this property is never reflected in the database
        public string DueDateCurrentCulture
        {
            get
            {
                return $"{DueDate:d}";
            }
        }

        // [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }
        public enum StatusEnum { Pending = 1, Done = 2, Delegated = 3 };

        public override string ToString()
        {
            return $"{Task} on {DueDate:d} is {Status}";
        }
    }
}
