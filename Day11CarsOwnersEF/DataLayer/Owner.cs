﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF.DataLayer
{
    public class Owner
    {
        public Owner()
        {
            // this is not strictly required but it simplifies some of our code later on
            // without it CarsInGarabe will be null (not an empty collection) if there are no Cars
            CarsInGarage = new HashSet<Car>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [NotMapped]
        public int CarsNumber { get => CarsInGarage.Count(); }
        [Required]
        public byte[] Photo { get; set; }

        // lazily loaded by default, we need to ask for it specifically if we want it fetched
        public virtual ICollection<Car> CarsInGarage { set; get; }

        public override string ToString()
        {
            return $"{Id}, {Name}";
        }
    }
}
