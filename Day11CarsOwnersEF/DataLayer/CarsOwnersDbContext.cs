﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF.DataLayer
{
    public class CarsOwnersDbContext : DbContext
    {
        const string DbName = "Day11CarsOwnersEFDb.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public CarsOwnersDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
    }
}
