﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        DataLayer.Owner currOwner;

        public CarsDialog(DataLayer.Owner owner)
        {
            InitializeComponent();
            currOwner = owner;
            lblOwnerName.Content = currOwner.Name;
            ReloadCarRecordsForCurrOwner();
        }

        void ReloadCarRecordsForCurrOwner()
        {
            try
            {
                // we don't necessarily need to run a query here because the data has been loaded already
                List<DataLayer.Car> carList = currOwner.CarsInGarage.ToList<DataLayer.Car>();
                lvDialogCars.ItemsSource = carList;
                Utils.AutoResizeColumns(lvDialogCars);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        bool AreInputsValid()
        {
            if (tbDialogMakeModel.Text.Length < 1 || tbDialogMakeModel.Text.Length > 100)
            {
                MessageBox.Show("Please, fill in Make & Model", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        public void ClearDialogInputs()
        {
            tbDialogMakeModel.Text = "";
            lblDialogId.Content = "";
            btnDialogDelete.IsEnabled = false;
            btnDialogUpdate.IsEnabled = false;
        }

        private void btnDialogAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            try
            {
                DataLayer.Car c = new DataLayer.Car
                {
                    MakeModel = tbDialogMakeModel.Text,
                    OwnerId = currOwner.Id
                };
                Globals.ctx.Cars.Add(c);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnDialogDone_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void lvDialogCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvDialogCars.SelectedIndex == -1)
            {
                ClearDialogInputs();
                return;
            }
            DataLayer.Car c = (DataLayer.Car)lvDialogCars.SelectedItem;
            lblDialogId.Content = c.Id;
            tbDialogMakeModel.Text = c.MakeModel;
            btnDialogDelete.IsEnabled = true;
            btnDialogUpdate.IsEnabled = true;
        }

        private void btnDialogUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            DataLayer.Car carCurr = (DataLayer.Car)lvDialogCars.SelectedItem;
            if (carCurr == null) { return; } // should never happen
            try
            {
                carCurr.MakeModel = tbDialogMakeModel.Text;
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
        private void btnDialogDelete_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Car carCurr = (DataLayer.Car)lvDialogCars.SelectedItem;
            if (carCurr == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + carCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
                { return; } // cancelled
            try
            {
                Globals.ctx.Cars.Remove(carCurr);
                Globals.ctx.SaveChanges();
                ClearDialogInputs();
                ReloadCarRecordsForCurrOwner();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}
