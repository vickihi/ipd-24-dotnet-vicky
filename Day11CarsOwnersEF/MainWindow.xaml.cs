﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarsOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>



    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new DataLayer.CarsOwnersDbContext(); // ex SystemException
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        public void ReloadRecords()
        {
            try
            {
                // if we don't make CarsInGarage virtual we proabably need to load them eagerly using Includ()
                // lvOwners.ItemsSource = Globals.ctx.Owners.Include("CarsInGarage").ToList(); // ex SystemException
                lvOwners.ItemsSource = Globals.ctx.Owners.ToList(); // ex SystemException
                Utils.AutoResizeColumns(lvOwners);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        bool AreInputsValid()
        {
            List<string> errorsList = new List<string>();
            if (tbName.Text.Length < 2 || tbName.Text.Length > 100)
            {
                errorsList.Add("Name must be between 2 and 100 characters");
            }
            if (currOwnerImage == null)
            {
                errorsList.Add("You must choose a picture");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        byte[] currOwnerImage; // currently selected image, null if none

        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            // dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currOwnerImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currOwnerImage); // ex: SystemException
                    imageViewer.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public void ClearInputs()
        {
            tbName.Text = "";
            imageViewer.Source = null;
            btnDelete.IsEnabled = false;
            btnUpdate.IsEnabled = false;
            btnManageCars.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) return;
            try
            {
                DataLayer.Owner owner = new DataLayer.Owner
                {
                    Name = tbName.Text,
                    Photo = currOwnerImage,
                    // CarsInGarage = new List<DataLayer.Car>()
                };
                Globals.ctx.Owners.Add(owner); // ex SystemException
                Globals.ctx.SaveChanges(); // ex SystemException
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnManageCars_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Owner owner = (DataLayer.Owner)lvOwners.SelectedItem;
            if (owner == null) return;
            CarsDialog carDialog = new CarsDialog(owner) { Owner = this };
            carDialog.ShowDialog();
            ReloadRecords();
        }
        private void lvOwners_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvOwners.SelectedIndex == -1)
            {
                ClearInputs();
                return;
            }
            try
            {
                DataLayer.Owner owner = (DataLayer.Owner)lvOwners.SelectedItem;
                lblOwnerId.Content = owner.Id;
                tbName.Text = owner.Name;
                currOwnerImage = owner.Photo;
                imageViewer.Source = Utils.ByteArrayToBitmapImage(owner.Photo); // ex
                btnDelete.IsEnabled = true;
                btnUpdate.IsEnabled = true;
                btnManageCars.IsEnabled = true;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            DataLayer.Owner ownerCurr = (DataLayer.Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            try
            {
                ownerCurr.Name = tbName.Text;
                ownerCurr.Photo = currOwnerImage;
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DataLayer.Owner ownerCurr = (DataLayer.Owner)lvOwners.SelectedItem;
            if (ownerCurr == null) { return; } // should never happen

            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the record?\n" + ownerCurr, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
                { return; } // action cancelled
            try
            {
                Globals.ctx.Owners.Remove(ownerCurr);
                Globals.ctx.SaveChanges();
                ClearInputs();
                ReloadRecords();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnExportSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvOwners.SelectedItems;
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            SaveFileDialog exportDialog = new SaveFileDialog();
            exportDialog.Filter = "CSV file (*.csv)|*.csv| All Files (*.*)|*.*";
            exportDialog.Title = "Export to file";
            exportDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

            if (exportDialog.ShowDialog() == true)
            {
                try
                {
                    // FIXME: force quoting
                    var config = new CsvConfiguration(CultureInfo.InvariantCulture) { Delimiter = ";", Encoding = Encoding.UTF8, HasHeaderRecord = true };
                    // FIXME: find out how to force date format in the new csvhelper
                    // var options = new TypeConverterOptions { Formats = new[] { "yyyy-MM-dd" } };
                    //csv.Configuration.TypeConverterOptionsCache.AddOptions<DateTime>(options);
                    using (StreamWriter writer = new StreamWriter(exportDialog.FileName))
                    using (CsvWriter csv = new CsvWriter(writer, config))
                    {
                        csv.Context.RegisterClassMap<OwnerExportMap>();
                        csv.WriteRecords(selItems);
                    }
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error exporting to csv: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }

    public sealed class OwnerExportMap : ClassMap<DataLayer.Owner>
    {
        public OwnerExportMap()
        {
            Map(m => m.Id);
            Map(m => m.Name);
            Map(m => m.CarsNumber);
        }
    }

}



